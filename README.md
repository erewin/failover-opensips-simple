# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

  Opensips 1.10 installed from yum repository on centos 7
  Also you need mysql server

### How do I get set up? ###

* How to works
 
 opensips proxy all requests (REGISTER, INVITE... OTHERS)
 opensips send keepalive OPTIONS to gateways to check if it online or down.
 There are groups for gateways, each group is client. client can have few gateways.
 Current opensips route messages without any logic just in order.

* add new client
    opensipsctl db exec "insert into domain_dispatcher(domain,dispatcher_group) values ('new_domain','2')"
    opensipsctl dispatcher addgw 2 sip:1.1.1.1:5060 '' 02 0 'g1' 'PBX1'
    opensipsctl dispatcher addgw 2 sip:2.2.2.2:5060 '' 02 0 'g1' 'PBX1'
       02 means Probing status
    opensipsctl dispatcher reload
    
* check gateways

    opensipsctl dispatcher dump
    
